package edu.byu.hbll.filediscovery;

import static org.junit.Assert.*;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.AfterClass;
import org.junit.Test;

public class FileDiscoveryTest {

  FileDiscovery fileDiscovery;

  @Test
  public void test() {
    Path source = Paths.get("src/test/resources");
    Path destination = Paths.get("src/test/resources/xls");
    fileDiscovery = new FileDiscovery(source, destination);
    fileDiscovery.run();

    assertTrue(destination.toFile().listFiles().length != 0);
    assertEquals(
        fileDiscovery.listWorksheetHeaders(),
        "FILENAME ITEM_ID FILEPATH COLLECTION DATE_CREATED DATE_MODIFIED TITLE CREATOR DESCRIPTION RIGHTS_POLICY RETENTION_POLICY BYTESIZE SIZE IE_LEVEL");
  }

  @AfterClass
  public static void finalTeardown() {
    // remove all *.xsl from "src/test/resources/xls"
    File dir = new File("src/test/resources/xls");

    for (File file : dir.listFiles()) if (!file.isDirectory()) file.delete();
  }
}
