package edu.byu.hbll.filediscovery;

import static java.nio.file.FileVisitResult.CONTINUE;
import static java.nio.file.FileVisitResult.TERMINATE;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringJoiner;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Discovers all files in and below sourceDir. Represents them as row in .xlsx. Checks for
 * non-accessible files. Limits the # of files to excel 2007 spec of 1.04 million rows
 *
 * <p>Application is built as stand alone jar
 *
 * @author Eric Nord
 * @version 1.0.2
 */

// TODO: consider implementing FileInformationToolSet (FITS): http://projects.iq.harvard.edu/fits
// TODO: implement metadata-extractor for images

public class FileDiscovery extends SimpleFileVisitor<Path> {
  private static Logger logger = LoggerFactory.getLogger(FileDiscovery.class);

  private static Path sourceDir;
  private static Path outputDir;
  private Workbook wb;
  private CreationHelper createHelper;
  private CellStyle cellDateStyle;
  private Sheet sheet;

  /**
   * Collects locations that are not accessible due to file system permissions. Used for error
   * reporting
   */
  private ArrayList<Path> accessDeniedPaths;

  /**
   * Creates new FileDiscovery object.
   *
   * @param sourceDir Directory that will be scanned for files
   * @param outputDir Directory where the resultant .xslx file will be placed
   */
  public FileDiscovery(Path sourceDir, Path outputDir) {
    this.wb = new XSSFWorkbook();
    this.sheet = wb.createSheet();
    this.accessDeniedPaths = new ArrayList<>();
    FileDiscovery.sourceDir = sourceDir;
    FileDiscovery.outputDir = outputDir;
    createHelper = wb.getCreationHelper();
    cellDateStyle = wb.createCellStyle();
    cellDateStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
  }

  /**
   * FileDiscovery.java takes 1 or 2 params.
   *
   * @param args [0] Directory to scan for files [1] Directory where .xslx will be saved
   */
  public static void main(String[] args) {
    final String ARGSERROR =
        "2 inputs required:\n "
            + "1- target directory of folder to scan\n "
            + "2 - destination directory to place .xslx file\n"
            + "Example: java -jar FileDiscovery.jar C:\\FolderToProcess\\ C:\\Users\\username\\Desktop \n";

    if (args.length == 2) {
      sourceDir = Paths.get(args[0]);
      outputDir = Paths.get(args[1]);
    } else {
      logger.error(ARGSERROR);
      System.exit(0);
    }

    //Verify sourceDir and outputDir exist up front
    if (Files.notExists(sourceDir)) {
      logger.error(sourceDir + " is not a valid directory");
    } else if (Files.notExists(outputDir)) {
      logger.error(outputDir + " is not a valid directory");
    }

    FileDiscovery discovery = new FileDiscovery(sourceDir, outputDir);
    discovery.run();
  }

  /** Creates workbook worksheet and populates header row as row 0. */
  @SuppressWarnings("checkstyle:magicnumber")
  public void run() {

    // Build the worksheet
    wb.setSheetName(0, "filelist");
    CreationHelper createHelper = wb.getCreationHelper();
    CellStyle cellStyle = wb.createCellStyle();
    cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));

    // Setup 0th row (header row)
    Row headerRow = sheet.createRow(0);
    headerRow.createCell(0).setCellValue("FILENAME");
    headerRow.createCell(1).setCellValue("ITEM_ID");
    headerRow.createCell(2).setCellValue("FILEPATH");
    headerRow.createCell(3).setCellValue("COLLECTION");
    headerRow.createCell(4).setCellValue("DATE_CREATED");
    headerRow.createCell(5).setCellValue("DATE_MODIFIED");
    headerRow.createCell(6).setCellValue("TITLE");
    headerRow.createCell(7).setCellValue("CREATOR");
    headerRow.createCell(8).setCellValue("DESCRIPTION");
    headerRow.createCell(9).setCellValue("RIGHTS_POLICY");
    headerRow.createCell(10).setCellValue("RETENTION_POLICY");
    headerRow.createCell(11).setCellValue("BYTESIZE");
    headerRow.createCell(12).setCellValue("SIZE");
    headerRow.createCell(13).setCellValue("IE_LEVEL");

    try {
      logger.info("Target directory found and readable: " + sourceDir);
      Files.walkFileTree(sourceDir, this);

      saveWorkbook(outputDir, wb);

      if (accessDeniedPaths.size() > 0) {
        logger.warn(
            "Inaccessible file count: "
                + accessDeniedPaths.size()
                + "\n"
                + accessDeniedPaths.toString());
      }

    } catch (IOException e) {
      logger.warn("Target directory " + sourceDir + " invalid " + e);
    }
  }

  /**
   * Gathers data for each file and represents data as row in worksheetPrint filename path and
   * unique id via full path hash for each file.
   *
   * @param file Path to current file
   * @param attr attributes of the current file
   * @return CONTINUE on successful write to new row. TERMINATE if sheet is out of rows (~1.04M)
   */
  @Override
  @SuppressWarnings("checkstyle:magicnumber")
  public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {

    //Sets style for date fields
    wb.setSheetName(0, "filelist");

    int newRow = sheet.getLastRowNum() + 1;
    logger.info("Processing file " + (newRow) + " : " + file);
    // ImageDiscovery imagedata = new ImageDiscovery(file);
    try {
      Row row = sheet.createRow(newRow);
      row.createCell(0).setCellValue(file.getFileName().toString());
      row.createCell(1).setCellValue(newRow);
      row.createCell(2).setCellValue(file.getParent().toString());
      row.createCell(4)
          .setCellValue(
              DateTimeFormatter.ofPattern("MM/dd/yyyy")
                  .withZone(ZoneId.systemDefault())
                  .format(attr.creationTime().toInstant()));
      row.getCell(4).setCellStyle(cellDateStyle);
      row.createCell(5)
          .setCellValue(
              DateTimeFormatter.ofPattern("MM/dd/yyyy")
                  .withZone(ZoneId.systemDefault())
                  .format(attr.lastModifiedTime().toInstant()));
      row.getCell(5).setCellStyle(cellDateStyle);
      row.createCell(11).setCellValue(attr.size());
      row.createCell(12).setCellValue(humanReadableByteCount(attr.size(), true));

      return CONTINUE;

    } catch (IllegalArgumentException e) {
      logger.error(
          "This collection of folders exceeds the allowable number of Excel 2007 rows of 1.04 Million \n"
              + "Please target subdirectories and try again. ");
      return TERMINATE;
    }
  }

  /**
   * Reports failed file visits.
   *
   * @param file Path to file
   * @param e Exception reported by visitFile()
   * @return CONTINUE as we desire the fileVisit to continue exploring the file tree
   */
  @Override
  public FileVisitResult visitFileFailed(Path file, IOException e) {
    try (PrintWriter out =
        new PrintWriter(
            new BufferedWriter(new FileWriter(outputDir + "/InaccessibleFiles.txt", true)))) {
      out.println(file);
    } catch (IOException f) {
      logger.info("Writing " + outputDir + "/InaccessibleFiles.log failed" + f);
    }

    accessDeniedPaths.add(file);

    return CONTINUE;
  }

  /**
   * Writes a prepared Excel 2007 object to output directory as .xslx.
   *
   * @param outDir Directory to save file to
   * @param workbook Excel 2007 workbook file to be saved
   * @throws IOException if path is invalid
   */
  private void saveWorkbook(Path outDir, Workbook workbook) throws IOException {
    // checks for null param values
    if (outDir == null || workbook == null) {
      throw new NullPointerException("could not save workbook invalid parameters");
    }

    String date = new SimpleDateFormat("yyyyMMddHHmm").format(new Date());
    String file = outputDir + "/fileList_" + date + ".xlsx";
    logger.info("Writing file to " + file);

    // Writes xslx file
    FileOutputStream out = new FileOutputStream(file);
    workbook.write(out);
    out.close();
  }

  /**
   * Provides human readable size from bytesize.
   *
   * @param bytes # of bytes
   * @param si Expected units of measure
   * @return human readable file size
   */
  private String humanReadableByteCount(long bytes, boolean si) {
    final int UNITSI = 1000;
    final int UNITBINARY = 1024;

    int unit = si ? UNITSI : UNITBINARY;
    if (bytes < unit) {
      return bytes + " B";
    }
    int exp = (int) (Math.log(bytes) / Math.log(unit));
    String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
    return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
  }

  /**
   * Returns column headers as string for testing.
   *
   * @return String of header values
   */
  public String listWorksheetHeaders() {
    StringJoiner sj = new StringJoiner(" ");
    for (Cell c : wb.getSheetAt(0).getRow(0)) {
      sj.add(c.getStringCellValue());
    }
    return sj.toString();
  }
}
